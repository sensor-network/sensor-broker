# SensorBroker
The SensorBroker reads data from a temperature/humidity sensor and provides the data by means of an REST-API.

Here, we use an DHT11 sensor which must be attached to GPIO pin no. 4 of your Raspberry Pi.

The sensor communication is realized by the [Adafruit Python DHT Sensor Library](https://github.com/adafruit/Adafruit_Python_DHT).\
The REST-API relies on [Flask](https://flask.palletsprojects.com).

Let's get started:

## Clone Repository
```
git clone git@gitlab.com:sensor-network/sensor-broker.git
```

## Build Locally
```
docker build -t sensor-broker .
```

## Run 
```
docker run --name sensor-broker --rm -d --privileged -p 5000:5000 sensor-broker
```

## Receive Data
```
http://hostname:5000/sensor
```
