import Adafruit_DHT
from decimal import Decimal
import time

from flask import Flask

app = Flask(__name__)


@app.route("/sensor")
def sensor():

    output = {}
    output['timestamp'] = round(Decimal(time.time()))
    output['humidity'], output['temperature'] = Adafruit_DHT.read_retry(sensor=Adafruit_DHT.DHT11, pin=4)

    return output
