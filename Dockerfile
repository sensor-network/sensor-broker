# BUILDER
FROM arm32v6/python:3.6.7-alpine3.8 as builder

RUN apk --no-cache add git build-base

WORKDIR /home

RUN git clone https://github.com/adafruit/Adafruit_Python_DHT.git && \
	cd Adafruit_Python_DHT && \
	python setup.py install

COPY requirements.txt .
RUN pip install -r requirements.txt

COPY app.py .

## RUNNER
FROM arm32v6/python:3.6.7-alpine3.8

RUN apk --no-cache add ca-certificates

COPY --from=builder /usr/local/lib/python3.6 /usr/local/lib/python3.6
COPY --from=builder /home/ /home/

RUN pip install --upgrade pip

WORKDIR /home

CMD  ["python", "-m", "flask", "run", "--host=0.0.0.0"]

EXPOSE 5000
